import { Component, OnInit } from '@angular/core';
import { EventService } from '../../services/event.service';
import { Event } from '../../services/observables/event'
import { DatePipe } from '@angular/common';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { VenueService } from '../../services/venue.service';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from '../../services/loader/loader.service';

@Component({
  selector: 'app-admin-maintain-events',
  templateUrl: './admin-maintain-events.component.html',
  styleUrls: ['./admin-maintain-events.component.css']
})
export class AdminMaintainEventsComponent implements OnInit {

  newEvent;
  editedEvent;
  newEventModal: NgbModalRef;
  editEventModal: NgbModalRef;
  pageNbr: number = 1;

  constructor(
    public eventService: EventService,
    public venueService: VenueService,
    private datePipe: DatePipe,
    private modalService: NgbModal,
    private loader: LoaderService
  ) { }

  ngOnInit() {
    this.getEvents();
    this.eventService.getEventCategories();
  }

  openModal(modal, content) {
    switch (modal) {
      case "newEventModal":
        this.newEventModal = this.modalService.open(content, { backdrop: "static" });
        this.newEventModal.result.then(() => {
          this.newEvent = null;
        }, () => {
          this.newEvent = null;
        });
        break;
      case "editEventModal":
        this.editEventModal = this.modalService.open(content, { backdrop: "static" });
        this.editEventModal.result.then(() => {
          this.editedEvent = null;
        }, () => {
          this.editedEvent = null;
        });
        break;
    }
  }

  private getEvents() {
    this.eventService.getEventsWithVenueInfo();
    this.venueService.getAllEventVenues();
  }

  resetEditedEvent(): void {
    this.editedEvent = null;
    this.eventService.getEventsWithVenueInfo();
  }

  initNewModal() {
    this.newEvent = new Event();
    this.newEvent.SelectedVenueIndex = -1;
    this.newEvent.Status = 'AVAILABLE';
    this.newEvent.formctrl_eventTitle = new FormControl('', Validators.required);
    this.newEvent.formgrp_eventDateTime = new FormGroup({
      formctrl_eventDateTime: new FormControl('', Validators.required)
    });
  }

  private getIndexOfVenue(venueNumber): number {
    for (let i = 0; i < this.venueService.venues.length; i++) {
      if (this.venueService.venues[i].VenueNumber == venueNumber)
        return i;
    }
    return -1;
  }

  updateEvent() {

    if (this.eventValid(this.editedEvent)) {

      // Create custom obj to get rid of form controls
      let updatedEventObjLight = {
        'EventNumber': this.editedEvent.EventNumber,
        'EventDescription': this.editedEvent.formctrl_eventTitle.value,
        'updatedFormattedEventDateTime': this.datePipe.transform(this.editedEvent.formgrp_eventDateTime.controls.formctrl_eventDateTime.value, 'yyyy-MM-dd HH:mm:ss'),
        'AssociatedTicketCategory': this.editedEvent.AssociatedTicketCategory,
        'AssociatedVenueNumber': this.venueService.venues[this.editedEvent.SelectedVenueIndex].VenueNumber,
        'Status': this.editedEvent.Status
      };

      this.eventService.updateEvent(updatedEventObjLight).subscribe(
        () => {
          this.resetEditedEvent();
          this.editEventModal.close();
          this.eventService.getEventsWithVenueInfo();
          swal("Success!", "The event was successfully updated.", "success");
          this.loader.hide();
        },
        err => {
          swal("Error!", "An error occured while updating the event.", "error");
          console.error(err);
          this.loader.hide();
        }
      )
    }
    else
      swal("Error!", "Event details are invalid.", "error");
  }

  editEvent(eventNbr) {

    // Save copying
    this.editedEvent = JSON.parse(JSON.stringify(this.eventService.events[this.eventService.events.findIndex(e => e.EventNumber === eventNbr)]));

    this.editedEvent.SelectedVenueIndex = this.getIndexOfVenue(this.editedEvent.AssociatedVenueNumber);

    if (!this.editedEvent.formctrl_eventTitle) {
      this.editedEvent.formctrl_eventTitle = new FormControl('', Validators.required);
    }

    this.editedEvent.formctrl_eventTitle.setValue(this.editedEvent.EventDescription);

    if (!this.editedEvent.formgrp_eventDateTime) {
      this.editedEvent.formgrp_eventDateTime = new FormGroup({
        formctrl_eventDateTime: new FormControl('', Validators.required)
      });
    }

    const eventDate = new Date(
      this.datePipe.transform(
        this.editedEvent.EventDateTime.date.replace(" ", "T").split(".")[0], 'M/d/y h:mm a'
      )
    );

    this.editedEvent.formgrp_eventDateTime.controls.formctrl_eventDateTime.setValue(
      eventDate
    );
  }

  private buildModal(event) {
    let modaltext = event.EventNumber;
    modaltext += " | ";
    modaltext += event.AssociatedTicketCategory;
    modaltext += " | ";
    modaltext += event.EventDescription;
    modaltext += " | ";
    modaltext += this.datePipe.transform(
      event.EventDateTime.date.replace(" ", "T").split(".")[0], 'EEEE, M/d/y @ h:mm a'
    );
    modaltext += " | ";
    modaltext += event.Name;
    return modaltext;
  }

  deleteEvent(eventNbr) {
    const i = this.eventService.events.findIndex(e => e.EventNumber === eventNbr);
    const event = this.eventService.events[i];
    swal({
      title: "You are about to delete the following event:",
      text: this.buildModal(event),
      icon: "warning",
      buttons: [true, true],
      dangerMode: true
    })
      .then((willApprove) => {
        if (willApprove) {
          this.eventService.deleteEvent(i).subscribe(
            () => {
              this.eventService.getEventsWithVenueInfo();
              swal("Success!", "The event was successfully deleted.", "success");
              this.loader.hide();
            },
            err => {
              swal("Error!", "An error occured while deleting the event.", "error");
              console.error(err);
              this.loader.hide();
            }
          );
        }
      });
  }

  addEvent() {

    if (this.newEvent.SelectedVenueIndex > -1)
      this.newEvent.AssociatedVenueNumber = this.venueService.venues[this.newEvent.SelectedVenueIndex].VenueNumber;

    if (this.newEvent.formgrp_eventDateTime.controls.formctrl_eventDateTime)
      this.newEvent.formattedEventDateTime =
        this.datePipe.transform(this.newEvent.formgrp_eventDateTime.controls.formctrl_eventDateTime.value, 'yyyy-MM-dd HH:mm:ss');

    if (this.eventValid(this.newEvent)) {
      // Create custom obj to get rid of form controls
      let newEventWithOutFromCtrl = {
        'EventDescription': this.newEvent.formctrl_eventTitle.value,
        'formattedEventDateTime': this.newEvent.formattedEventDateTime,
        'AssociatedTicketCategory': this.newEvent.AssociatedTicketCategory,
        'AssociatedVenueNumber': this.newEvent.AssociatedVenueNumber,
        'Status': this.newEvent.Status
      };
      this.eventService.addEvent(newEventWithOutFromCtrl).subscribe(
        () => {
          this.eventService.getEventsWithVenueInfo();
          this.newEvent = null;
          this.newEventModal.close();
          swal("Success!", "The event was successfully saved.", "success");
          this.loader.hide();
        },
        err => {
          swal("Error!", "An error occured while saving the event.", "error");
          console.error(err);
          this.loader.hide();
        }
      );
    }
    else
      swal("Error!", "Event details are invalid.", "error");
  }

  preFillTitleFromCategory(category) {
    if (!this.newEvent.EventDescription || this.newEvent.EventDescription.length == 0) {
      let preFillValue = '';
      switch (category) {
        case "CAVS":
          preFillValue = 'Cavs Vs. ';
          break;
        case "BROWNS":
          preFillValue = 'Browns Vs. ';
          break;
        case "MONSTERS":
          preFillValue = 'Monsters Vs. ';
          break;
        case "INDIANS":
          preFillValue = 'Indians Vs. ';
          break;
      }
      this.newEvent.EventDescription = preFillValue;
    }
    else {
      if (this.newEvent.EventDescription == 'Cavs Vs. '
        || this.newEvent.EventDescription == 'Browns Vs. '
        || this.newEvent.EventDescription == 'Monsters Vs. '
        || this.newEvent.EventDescription == 'Indians Vs. ') {
        this.newEvent.EventDescription = null;
        this.preFillTitleFromCategory(category);
      }
    }
  }

  private eventValid(eventObj): Boolean {
    if (!eventObj.AssociatedTicketCategory)
      return false;
    if (!eventObj.AssociatedVenueNumber)
      return false;
    if (!eventObj.formctrl_eventTitle.valid)
      return false;
    if (!eventObj.formgrp_eventDateTime.controls.formctrl_eventDateTime.valid)
      return false;
    return true;
  }
}