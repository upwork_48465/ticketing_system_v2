export const environment = {
  envName: 'live',
  production: true,
  baseHref: '/',
  backendPath: 'https://ticketing.huntinglake.com/scripts/'
};
